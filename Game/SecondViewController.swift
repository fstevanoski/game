//
//  SecondViewController.swift
//  Game
//
//  Created by Pero on 5/9/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var descriptionLabel : UILabel!
    var wordLabel : UILabel!
    var wordTextField : UITextField!
    var timerString : String!
    var timerLabel : UILabel!
    var nextRoundButton : UIButton!
    var counter : Int!
    var timer : Timer!

    var wordsArray = ["alliteration", "unidentified", "intermittent", "Pennsylvania", "exacerbation", "independence", "commensalism", "intelligence", "relationship", "thanksgiving", "professional", "organization", "sporadically", "intimidating", "abolitionist", "onomatopoeia", "appreciation", "annunciation", "malnutrition", "architecture", "biodiversity", "acceleration", "interdiction", "trigonometry", "communicator", "bodybuilding", "perspiration", "appertaining", "resurrection", "constipation", "civilization", "velociraptor", "retrocession", "expectations", "ambidextrous", "cytoskeleton", "hippopotamus", "interjection", "exasperation", "felicitation", "abbreviation", "voluminosity", "rambunctious", "colonization", "interception", "championship", "acquaintance", "depreciation", "consequences", "grandparents"]
    
    init(timerString : String){
        super.init(nibName: nil, bundle: nil)
        self.timerString = timerString
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
   
    func setupView() {

        counter = Int(timerString)
        nextRoundButton = UIButton()
        nextRoundButton.setTitle("Next Round", for: .normal)
        nextRoundButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        nextRoundButton.setTitleColor(UIColor.white, for: .normal)
        nextRoundButton.addTarget(self, action: #selector(onClicknextRoundButtonButton), for: .touchUpInside)
        nextRoundButton.sizeToFit()
        
        timerLabel = UILabel()
        //timerLabel.text = timerString
        timerLabel.textAlignment = .center
        timerLabel.textColor = UIColor.white
        timerLabel.font = UIFont.boldSystemFont(ofSize: 25)
        timerLabel.sizeToFit()
        
        descriptionLabel = UILabel()
        descriptionLabel.text = "Rewrite the word"
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.backgroundColor = UIColor.clear
        descriptionLabel.font = UIFont.boldSystemFont(ofSize: 25)
        descriptionLabel.isHidden = true
        descriptionLabel.sizeToFit()
        
        wordLabel = UILabel()
        wordLabel.layer.masksToBounds = true
        let randomWord = Int(arc4random() % UInt32(wordsArray.count))
        wordLabel.text = "\(wordsArray[randomWord])".lowercased()
        //wordLabel.text = "a"
        wordLabel.textAlignment = .center
        wordLabel.textColor = UIColor.white
        wordLabel.backgroundColor = UIColor.clear
        wordLabel.layer.borderWidth = 3
        wordLabel.layer.cornerRadius = 20
        wordLabel.layer.borderColor = UIColor.white.cgColor
        wordLabel.isHidden = true
        wordLabel.font = UIFont.systemFont(ofSize: 22)
       
        wordTextField = UITextField()
        wordTextField.textAlignment = .center
        wordTextField.textColor = UIColor.white
        wordTextField.layer.borderWidth = 3
        wordTextField.layer.cornerRadius = 20
        wordTextField.layer.borderColor = UIColor.white.cgColor
        wordTextField.backgroundColor = UIColor.clear
        wordTextField.delegate = self
        wordTextField.autocorrectionType = .no
        wordTextField.isHidden = true
        
        self.view.addSubview(wordLabel)
        self.view.addSubview(descriptionLabel)
        self.view.addSubview(wordTextField)
        self.view.addSubview(timerLabel)
        self.view.addSubview(nextRoundButton)
        
    }
    
    func startTimer() {
        counter = counter + 1
        timerLabel.text = String(counter)
        
    }
    func onClicknextRoundButtonButton() {
        descriptionLabel.isHidden = false
        wordLabel.isHidden = false
        wordTextField.isHidden = false
        nextRoundButton.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector (startTimer), userInfo: nil, repeats: true)
    }
    
    func setupConstraints() {
        nextRoundButton.snp.makeConstraints { make in
            make.center.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(30)
        }
        timerLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(30)
            make.centerX.equalTo(self.view)
            make.width.equalTo(50)
            make.height.equalTo(20)
        }
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view.frame.size.height/7)
            make.centerX.equalTo(self.view)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(50)
        }
        wordLabel.snp.makeConstraints { make in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.5)
            make.height.equalTo(50)
        }
        wordTextField.snp.makeConstraints { make in
            make.top.equalTo(wordLabel.snp.bottom).offset(50)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(wordLabel)
            make.height.equalTo(wordLabel)
        }
    }
}

extension SecondViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.text?.lowercased() != wordLabel.text {
            textField.text = ""
            let color =  UIColor.lightGray
            textField.placeholder = "TRY AGAIN"
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes:[NSForegroundColorAttributeName : color])
            wordLabel.textColor = UIColor.lightGray
            wordLabel.font = UIFont.systemFont(ofSize: 16)
        } else if textField.text?.lowercased() == wordLabel.text {
            textField.resignFirstResponder()
            timer.invalidate()
            self.present(ThirdViewController(timer : timerLabel.text!), animated: true, completion: nil)
        }
        return true
    }
}
