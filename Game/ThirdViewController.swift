//
//  ThirdViewController.swift
//  Game
//
//  Created by Pero on 5/9/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import UserNotifications

class ThirdViewController: UIViewController {
    
    var questionsArray = ["Skopje is the capital of Mk","Football is a sport","Table is a color","New York is in USA", "Water is liquid","BMW is a car model"]
    var answerArray = ["yes","yes","no","yes","yes","yes"]
    var label : UILabel!
    var labeltext : String!
    var playAgainButton : UIButton!
    var timer : String!
    var timerLabel : UILabel!
    var qa = [QandA]()
   
    init(timer : String){
        super.init(nibName: nil, bundle: nil)
        self.timer = timer
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        
        for i in 0...questionsArray.count - 1 {
            qa.append(QandA(question: questionsArray[i], answer: answerArray[i]))
        }
        
        getNotification()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        NotificationCenter.default.addObserver(self, selector: #selector(changeLabelText), name: Notification.Name.showLabel, object: nil)
        
        }
    func changeLabelText() {
        label.text = UserDefaults.standard.string(forKey: "NewCongrats")
        timerLabel.text = "Time: " + UserDefaults.standard.string(forKey: "Timer")! + " sec"
        playAgainButton.isHidden = false
        animateButton()
        }
    
    func setupView() {
        UserDefaults.standard.set(Int(timer), forKey: "Timer")
        timerLabel = UILabel()
        timerLabel.textAlignment = .center
        timerLabel.font = UIFont.boldSystemFont(ofSize: 25)
        timerLabel.textColor = UIColor.white
        timerLabel.sizeToFit()
        
        label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 40)
        label.textColor = UIColor.white
        
        playAgainButton = UIButton()
        playAgainButton.setTitle("Play Again", for: .normal)
        playAgainButton.setTitleColor(UIColor.white, for: .normal)
        playAgainButton.addTarget(self, action: #selector(onPlayAgainPressed), for: .touchUpInside)
        playAgainButton.isHidden = true
        
        self.view.addSubview(label)
        self.view.addSubview(timerLabel)
        self.view.addSubview(playAgainButton)
    }
    
    func getNotification() {
        let randomWord = Int(arc4random() % UInt32(questionsArray.count))
        let randomQuestion = qa[randomWord].question
        
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Answer the question", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: randomQuestion!, arguments: nil)
        content.userInfo = ["odgovor":qa[randomWord].answer]
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "notify-test"
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(150)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view)
            make.height.equalTo(45)
        }
        timerLabel.snp.makeConstraints { make in
            make.top.equalTo(label.snp.bottom).offset(20)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(2)
            make.height.equalTo(label)
        }
        playAgainButton.snp.makeConstraints{make in
            make.bottom.equalTo(self.view).offset(-40)
            make.centerX.equalTo(self.view)
            make.width.equalTo(self.view.snp.width).dividedBy(2.7)
        }
    }
    
    func onPlayAgainPressed() {
        self.present(ViewController(), animated: true, completion: nil)
    }
    
    func animateButton() {
        UIView.animate(withDuration: 0.6,
                       animations: {
        self.playAgainButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
        completion: { _ in
        UIView.animate(withDuration: 2.6) {
        self.playAgainButton.transform = CGAffineTransform.identity
        }
        })
    }
}
extension ThirdViewController : UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.badge])
    }
    
}

extension Notification.Name {
    static let showLabel = Notification.Name("showLabel")
}
