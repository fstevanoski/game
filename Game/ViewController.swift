//
//  ViewController.swift
//  Game
//
//  Created by Pero on 5/9/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import SnapKit
import UserNotifications

class ViewController: UIViewController {
    
    var wordLabel : UILabel!
    var textLabel : UILabel!
    var startButton : UIButton!
    var gameButton : UIButton!
    var timer : Timer!
    var counter = 0
    var timerLabel : UILabel!
    var nextLevelCounter = 0
    var left : Int!
    var top : Int!
    var ControlOneLeft : Int!
    var controlOneTop : Int!
    var controlTwoLeft : Int!
    var controlTwoTop : Int!
    var id : Int!
    var buttonTitle = ["Here I am", "Catch me", "Be faster", "Click me","PressMe"]
    var randomChoice = Int(arc4random_uniform(5))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIDevice.current.orientation.isLandscape {
            constraintsLandscape()
        } else {
            setupConstraints()
        }
    }
    
    func setupViews() {
        
        textLabel = UILabel()
        textLabel.textColor = UIColor.white
        textLabel.textAlignment = .center
        textLabel.font = UIFont.boldSystemFont(ofSize: 20)
        textLabel.sizeToFit()
        
        timerLabel = UILabel()
        timerLabel.font = UIFont.boldSystemFont(ofSize: 25)
        timerLabel.textColor = UIColor.white
        timerLabel.textAlignment = .center
        timerLabel.text = String(counter)
        timerLabel.sizeToFit()
        timerLabel.isHidden = true
        
        startButton = UIButton()
        startButton.setTitle("PLAY GAME", for: .normal)
        startButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        startButton.setTitleColor(UIColor.white, for: .normal)
        startButton.addTarget(self, action: #selector(onClickStartButton), for: .touchUpInside)
        
        gameButton = UIButton()
        gameButton.setTitle(buttonTitle[randomChoice], for: .normal)
        gameButton.layer.backgroundColor = UIColor.clear.cgColor
        gameButton.setTitleColor(UIColor.white, for: .normal)
        gameButton.addTarget(self, action: #selector(onClickgameButton), for: .touchUpInside)
        gameButton.layer.cornerRadius = 40
        gameButton.isHidden = true
        gameButton.layer.borderWidth = 2
        gameButton.layer.borderColor = UIColor.white.cgColor
        gameButton.isUserInteractionEnabled = false
        
        self.view.addSubview(gameButton)
        self.view.addSubview(startButton)
        self.view.addSubview(timerLabel)
    }
    
   override func touchesBegan(_ _touches: Set<UITouch>, with event : UIEvent?) {
        let touch = _touches.first
        let touchLocation = touch?.location(in: self.view)
        if gameButton.layer.presentation()?.hitTest(touchLocation!) != nil {
            gameButton.layer.removeAllAnimations()
            onClickgameButton()
            animate()
        }
    }
    
    func setupConstraints() {
        startButton.snp.makeConstraints { make in
            make.center.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(30)
        }
        
        timerLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(30)
            make.centerX.equalTo(self.view)
            make.width.equalTo(50)
            make.height.equalTo(20)
        }
    }
    
    func constraintsLandscape() {
        startButton.snp.remakeConstraints {make in
            make.center.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(4)
            make.height.equalTo(30)
        }
        
        timerLabel.snp.remakeConstraints {make in
            make.top.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-10)
            make.width.equalTo(50)
            make.height.equalTo(20)
        }
    }
    
    func startTimer() {
        counter = counter + 1
        timerLabel.text = String(counter)
    }
    
    func onClickStartButton() {
        gameButton.isHidden = false
        startButton.isHidden = true
        timerLabel.isHidden = false
        animate()
        gameButton.snp.makeConstraints{ make in
            make.top.equalTo(self.view).offset(180)
            make.left.equalTo(self.view).offset(22)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector (startTimer), userInfo: nil, repeats: true)
    }
    
    func onClickgameButton() {
        nextLevelCounter += 1
        if nextLevelCounter > 5  {
            gameButton.isHidden = true
            self.present(SecondViewController(timerString : timerLabel.text!), animated: true, completion: nil)
        }
    }
    
    func animate() {
    UIView.animate(withDuration: 0,
    delay: 0,
    options: .allowUserInteraction,
    animations:
    { () -> Void in
                self.left = Int(arc4random_uniform(UInt32(self.view.frame.width - 80)))
                self.top = Int(arc4random_uniform(UInt32(self.view.frame.height - 80)))
                self.ControlOneLeft = Int(arc4random_uniform(UInt32(self.view.frame.width - 80)))
                self.controlOneTop  = Int(arc4random_uniform(UInt32(self.view.frame.height - 80)))
                self.controlTwoLeft = Int(arc4random_uniform(UInt32(self.view.frame.width - 80)))
                self.controlTwoTop = Int(arc4random_uniform(UInt32(self.view.frame.height - 80)))
                let path = UIBezierPath()
                path.move(to: CGPoint(x: self.left, y: self.top))
                path.addCurve(to: CGPoint(x: self.left, y: self.top), controlPoint1: CGPoint(x: self.ControlOneLeft, y: self.controlOneTop), controlPoint2: CGPoint(x: self.controlTwoLeft, y:self.controlTwoTop))
                let anim = CAKeyframeAnimation(keyPath: "position")
                anim.path = path.cgPath
                anim.rotationMode = kCAAnimationRotateAuto
                anim.repeatCount = Float.infinity
                anim.duration = 5.0
        self.gameButton.layer.add(anim, forKey: "position")
    },completion: nil
    );}
}

