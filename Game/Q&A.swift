//
//  Q&A.swift
//  Game
//
//  Created by Pero on 5/11/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation

class QandA : NSObject {
    
    var question : String!
    var answer : String!
    
    init(question : String, answer : String){
        self.question = question
        self.answer = answer
        
    }
   
}
